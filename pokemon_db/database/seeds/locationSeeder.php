<?php

use Illuminate\Database\Seeder;

class locationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('location')->insert(array(

          array(
            'pokedex_id' => '1',
            'generation' => '7',
            'location' => 'Sun\Moon: Trade, US\UM: Route 2 (Island Scan) - Friday, LGP\LGE: Viridian Forest Cerulean City(only one)'
          ), array(
            'pokedex_id' => '2',
            'generation' => '7',
            'location' => 'Sun\Moon: Trade, US\UM: Evolve Bulbasaur, LGP\LGE: Evolve Bulbasaur'
          ), array(
            'pokedex_id' => '3',
            'generation' => '7',
            'location' => 'Sun\Moon: Trade, US\UM: Evolve Ivysaur, LGP\LGE: Evolve Ivysaur'
          )
        ));
    }
}
