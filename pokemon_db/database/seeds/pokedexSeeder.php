<?php

use Illuminate\Database\Seeder;

class pokedexSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pokedex')->insert(array(

          array(
            'name' => 'Bulbasaur',
            'pokedexNumber' => '1',
            'description' => 'While it is young, it uses the nutrients that are stored in the seed on its back in order to grow.',
          ), array(
            'name' => 'Ivysaur',
            'pokedexNumber' => '2',
            'description' => 'When the bulb on its back grows large, it appears to lose the ability to stand on its hind legs.',
          ), array(
            'name' => 'Venusaur',
            'pokedexNumber' => '3',
            'description' => 'A bewitching aroma wafts from its flower. The fragrance becalms those engaged in a battle.',
          ), array(
            'name' => 'Charmander',
            'pokedexNumber' => '4',
            'description' => 'The flame at the tip of its tail makes a sound as it burns. You can only hear it in quiet places.',
          ), array(
            'name' => 'Charmeleon',
            'pokedexNumber' => '5',
            'description' => 'It is very hot-headed by nature, so it constantly seeks opponents. It calms down only when it wins.',
          ), array(
            'name' => 'Charizard',
            'pokedexNumber' => '6',
            'description' => 'Its wings can carry this Pokémon close to an altitude of 4,600 feet. It blows out fire at very high temperatures.',
          ), array(
            'name' => 'Squirtle',
            'pokedexNumber' => '7',
            'description' => 'When it retracts its long neck into its shell, it squirts out water with vigorous force.',
          ), array(
            'name' => 'Wartortle',
            'pokedexNumber' => '8',
            'description' => 'It is recognized as a symbol of longevity. If its shell has algae on it, that Wartortle is very old.',
          ), array(
            'name' => 'Blastoise',
            'pokedexNumber' => '9',
            'description' => 'Once it takes aim at its enemy, it blasts out water with even more force than a fire hose.',
          ), array(
            'name' => 'Caterpie',
            'pokedexNumber' => '10',
            'description' => 'Perhaps because it would like to grow up quickly, it has a voracious appetite, eating a hundred leaves a day.',
          ),
        ));
    }
}
