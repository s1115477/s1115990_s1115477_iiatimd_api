<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;


class FirebaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $factory = (new Factory)->withServiceAccount(__DIR__.'/laravelfirebasepokemon-firebase-adminsdk-3zf5c-dcd1ced1cf.json');

      $database = $factory->createDatabase();

      $newPost = $database
      ->getReference('blog/posts')
      ->push([
      'title' => 'Laravel FireBase Tutorial' ,
      'category' => 'Laravel'
      ]);
       return response()->json($newPost->getvalue());

    }

    public function getPokedex(){
      $factory = (new Factory)->withServiceAccount(__DIR__.'/laravelfirebasepokemon-firebase-adminsdk-3zf5c-dcd1ced1cf.json');

      $database = $factory->createDatabase();

      $Info = $database
      ->getReference('Pokedex');
       return response()->json($Info->getvalue());
    }

    public function getLocation(){
      $factory = (new Factory)->withServiceAccount(__DIR__.'/laravelfirebasepokemon-firebase-adminsdk-3zf5c-dcd1ced1cf.json');

      $database = $factory->createDatabase();

      $Info = $database
      ->getReference('Location');
       return response()->json($Info->getvalue());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $factory = (new Factory)->withServiceAccount(__DIR__.'/laravelfirebasepokemon-firebase-adminsdk-3zf5c-dcd1ced1cf.json');

      $database = $factory->createDatabase();

      $newPost = $database
      ->getReference('Pokedex')
      ->push([
        array(
          'name' => 'Bulbasaur',
          'pokedexNumber' => '1',
          'description' => 'While it is young, it uses the nutrients that are stored in the seed on its back in order to grow.',
        ), array(
          'name' => 'Ivysaur',
          'pokedexNumber' => '2',
          'description' => 'When the bulb on its back grows large, it appears to lose the ability to stand on its hind legs.',
        ), array(
          'name' => 'Venusaur',
          'pokedexNumber' => '3',
          'description' => 'A bewitching aroma wafts from its flower. The fragrance becalms those engaged in a battle.',
        ), array(
          'name' => 'Charmander',
          'pokedexNumber' => '4',
          'description' => 'The flame at the tip of its tail makes a sound as it burns. You can only hear it in quiet places.',
        ), array(
          'name' => 'Charmeleon',
          'pokedexNumber' => '5',
          'description' => 'It is very hot-headed by nature, so it constantly seeks opponents. It calms down only when it wins.',
        ), array(
          'name' => 'Charizard',
          'pokedexNumber' => '6',
          'description' => 'Its wings can carry this Pokémon close to an altitude of 4,600 feet. It blows out fire at very high temperatures.',
        ), array(
          'name' => 'Squirtle',
          'pokedexNumber' => '7',
          'description' => 'When it retracts its long neck into its shell, it squirts out water with vigorous force.',
        ), array(
          'name' => 'Wartortle',
          'pokedexNumber' => '8',
          'description' => 'It is recognized as a symbol of longevity. If its shell has algae on it, that Wartortle is very old.',
        ), array(
          'name' => 'Blastoise',
          'pokedexNumber' => '9',
          'description' => 'Once it takes aim at its enemy, it blasts out water with even more force than a fire hose.',
        ), array(
          'name' => 'Caterpie',
          'pokedexNumber' => '10',
          'description' => 'Perhaps because it would like to grow up quickly, it has a voracious appetite, eating a hundred leaves a day.',
        ),array(
          'name' => 'Metapod',
          'pokedexNumber' => '11',
          'description' => 'Hardens its shell to protect itself. However, a large impact may cause it to pop out of its shell.'
        ),array(
          'name' => 'Butterfree',
          'pokedexNumber' => '12',
          'description' => 'Its wings, covered with poisonous powder, repel water. This allows it to fly in the rain.'
        ),array(
          'name' => 'Weedle',
          'pokedexNumber' => '13',
          'description' => 'Beware of the sharp stinger on its head. It hides in grass and bushes where it eats leaves.'
        ),array(
          'name' => 'Kakuna',
          'pokedexNumber' => '14',
          'description' => 'Able to move only slightly. When endangered, it may stick out its stinger and poison its enemy.'
        ),array(
          'name' => 'Beedrill',
          'pokedexNumber' => '15',
          'description' => 'Its legs have become poison stingers. It stabs its prey repeatedly with the stingers on its limbs, dealing the final blow with the stinger on its rear.'
        ),array(
          'name' => 'Pidgey',
          'pokedexNumber' => '16',
          'description' => 'Very docile. If attacked, it will often kick up sand to protect itself rather than fight back.'
        ),array(
          'name' => 'Pidgeotto',
          'pokedexNumber' => '17',
          'description' => 'This Pokémon is full of vitality. It constantly flies around its large territory in search of prey.'
        ),array(
          'name' => 'Pidgeot',
          'pokedexNumber' => '18',
          'description' => 'This Pokémon flies at Mach 2 speed, seeking prey. Its large talons are feared as wicked weapons.'
        ),array(
          'name' => 'Rattata',
          'pokedexNumber' => '19',
          'description' => 'Will chew on anything with its fangs. If you see one, you can be certain that 40 more live in the area.'
        ),array(
          'name' => 'Raticate',
          'pokedexNumber' => '20',
          'description' => 'Its hind feet are webbed. They act as flippers, so it can swim in rivers and hunt for prey.'
        ),array(
          'name' => 'Spearow',
          'pokedexNumber' => '21',
          'description' => 'Inept at flying high. However, it can fly around very fast to protect its territory.'
        ),array(
          'name' => 'Fearow',
          'pokedexNumber' => '22',
          'description' => 'A Pokémon that dates back many years. If it senses danger, it flies high and away, instantly.'
        ),array(
          'name' => 'Ekans',
          'pokedexNumber' => '23',
          'description' => 'The older it gets, the longer it grows. At night, it wraps its long body around tree branches to rest.'
        ),array(
          'name' => 'Arbok',
          'pokedexNumber' => '24',
          'description' => 'The frightening patterns on its belly have been studied. Six variations have been confirmed.'
        ),array(
          'name' => 'Pikachu',
          'pokedexNumber' => '25',
          'description' => "This forest-dwelling Pokémon stores electricity in its cheeks, so you'll feel a tingly shock if you touch it."
        ),array(
          'name' => 'Raichu',
          'pokedexNumber' => '26',
          'description' => "It loves pancakes prepared with a secret Alolan recipe. Some wonder whether that recipe holds the key to this Pokémon's evolution."
        ),array(
          'name' => 'Sandshrew',
          'pokedexNumber' => '27',
          'description' => 'Its body is dry. When it gets cold at night, its hide is said to become coated with a fine dew.'
        ),array(
          'name' => 'Sandslash',
          'pokedexNumber' => '28',
          'description' => 'It is skilled at slashing enemies with its claws. If broken, they start to grow back in a day.'
        ),array(
          'name' => 'Nidoran♀',
          'pokedexNumber' => '29',
          'description' => 'A mild-mannered Pokémon that does not like to fight. Beware–its small horn secretes venom.'
        ),array(
          'name' => 'Nidorina',
          'pokedexNumber' => '30',
          'description' => 'When resting deep in its burrow, its barbs always retract. This is proof that it is relaxed.'
        ),array(
          'name' => 'Nidoqueen',
          'pokedexNumber' => '31',
          'description' => 'Tough scales cover the sturdy body of this Pokémon. It appears that the scales grow in cycles.'
        ),array(
          'name' => 'Nidoran♂',
          'pokedexNumber' => '32',
          'description' => 'Its large ears are always kept upright. If it senses danger, it will attack with a poisonous sting.'
        ),array(
          'name' => 'Nidorino',
          'pokedexNumber' => '33',
          'description' => 'Its horn contains venom. If it stabs an enemy with the horn, the impact makes the poison leak out.'
        ),array(
          'name' => 'Nidoking',
          'pokedexNumber' => '34',
          'description' => 'Its steel-like hide adds to its powerful tackle. Its horns are so hard, they can pierce a diamond.'
        ),array(
          'name' => 'Clefairy',
          'pokedexNumber' => '35',
          'description' => 'Adored for their cute looks and playfulness. They are thought to be rare, as they do not appear often.'
        ),array(
          'name' => 'Clefable',
          'pokedexNumber' => '36',
          'description' => 'They appear to be very protective of their own world. It is a kind of fairy, rarely seen by people.'
        ),array(
          'name' => 'Vulpix',
          'pokedexNumber' => '37',
          'description' => 'Both its fur and its tails are beautiful. As it grows, the tails split and form more tails'
        ),array(
          'name' => 'Ninetales',
          'pokedexNumber' => '38',
          'description' => 'It lives on mountains perpetually covered in snow and is revered as a deity incarnate. It appears draped in a blizzard.'
        ),array(
          'name' => 'Jigglypuff',
          'pokedexNumber' => '39',
          'description' => 'Uses its cute round eyes to enrapture its foe. It then sings a pleasing melody that lulls the foe to sleep.'
        ),array(
          'name' => 'Wigglytuff',
          'pokedexNumber' => '40',
          'description' => 'Its body is very elastic. By inhaling deeply, it can continue to inflate itself without limit.'
        ),array(
          'name' => 'Zubat',
          'pokedexNumber' => '41',
          'description' => 'Emits ultrasonic cries while it flies. They act as a sonar used to check for objects in its way.'
        ),array(
          'name' => 'Golbat',
          'pokedexNumber' => '42',
          'description' => 'It attacks in a stealthy manner, without warning. Its sharp fangs are used to bite and to suck blood.'
        ),array(
          'name' => 'Oddish',
          'pokedexNumber' => '43',
          'description' => 'It may be mistaken for a clump of weeds. If you try to yank it out of the ground, it shrieks horribly.'
        ),array(
          'name' => 'Gloom',
          'pokedexNumber' => '44',
          'description' => 'Smells incredibly foul! However, around one out of a thousand people enjoy sniffing its nose-bending stink.'
        ),array(
          'name' => 'Vileplume',
          'pokedexNumber' => '45',
          'description' => 'Flaps its broad flower petals to scatter its poisonous pollen. The flapping sound is very loud.'
        ),array(
          'name' => 'Paras',
          'pokedexNumber' => '46',
          'description' => 'Burrows under the ground to gnaw on tree roots. The mushrooms on its back absorb most of the nutrition.'
        ),array(
          'name' => 'Parasect',
          'pokedexNumber' => '47',
          'description' => 'The bug host is drained of energy by the mushrooms on its back. The mushroom appears to do all the thinking.'
        ),array(
          'name' => 'Venonat',
          'pokedexNumber' => '48',
          'description' => 'Its large eyes act as a radar. In a bright place, you can see that they are clusters of many tiny eyes.'
        ),array(
          'name' => 'Venomoth',
          'pokedexNumber' => '49',
          'description' => 'The powdery scales on its wings are hard to remove from skin. They also contain poison that leaks out on contact.'
        ),array(
          'name' => 'Diglett',
          'pokedexNumber' => '50',
          'description' => 'It prefers dark places. It spends most of its time underground, though it may pop up in caves.'
        ),array(
          'name' => 'Dugtrio',
          'pokedexNumber' => '51',
          'description' => "A team of triplets that can burrow to a depth of 60 miles. It's reported that this triggers an earthquake."
        ),array(
          'name' => 'Meowth',
          'pokedexNumber' => '52',
          'description' => "Appears to be more active at night. It loves round and shiny things. It can't stop itself from picking them up."
        ),array(
          'name' => 'Persian',
          'pokedexNumber' => '53',
          'description' => 'The gem in its forehead glows on its own! It walks with all the grace and elegance of a proud queen.'
        ),array(
          'name' => 'Psyduck',
          'pokedexNumber' => '54',
          'description' => 'Always tormented by headaches. It uses psychic powers, but whether it intends to do so is not known.'
        ),array(
          'name' => 'Golduck',
          'pokedexNumber' => '55',
          'description' => 'Its long, slim limbs end in broad flippers. They are used for swimming gracefully in lakes.'
        ),array(
          'name' => 'Mankey',
          'pokedexNumber' => '56',
          'description' => 'An agile Pokémon that lives in trees. It angers easily and will not hesitate to attack anything.'
        ),array(
          'name' => 'Primeape',
          'pokedexNumber' => '57',
          'description' => 'It stops being angry only when nobody else is around. To view this moment is very difficult.'
        ),array(
          'name' => 'Growlithe',
          'pokedexNumber' => '58',
          'description' => 'A Pokémon with a friendly nature. However, it will bark fiercely at anything invading its territory.'
        ),array(
          'name' => 'Arcanine',
          'pokedexNumber' => '59',
          'description' => 'A legendary Pokémon in the East. Many people are charmed by the grace and beauty of its running.'
        ),array(
          'name' => 'Poliwag',
          'pokedexNumber' => '60',
          'description' => 'The direction of the spiral on the belly differs by area. It is more adept at swimming than walking.'
        ),array(
          'name' => 'Poliwhirl',
          'pokedexNumber' => '61',
          'description' => 'Under attack, it uses its belly spiral to put the foe to sleep. It then makes its escape.'
        ),array(
          'name' => 'Poliwrath',
          'pokedexNumber' => '62',
          'description' => 'Swims powerfully using all the muscles in its body. It can even overtake world-class swimmers.'
        ),array(
          'name' => 'Abra',
          'pokedexNumber' => '63',
          'description' => 'Sleeps 18 hours a day. If it senses danger, it will teleport itself to safety even as it sleeps'
        ),array(
          'name' => 'Kadabra',
          'pokedexNumber' => '64',
          'description' => 'Many odd things happen if this Pokémon is close by. For example, it makes clocks run backward.'
        ),array(
          'name' => 'Alakazam',
          'pokedexNumber' => '65',
          'description' => "A Pokémon that can memorize anything. It never forgets what it learns—that's why this Pokémon is smart."
        ),array(
          'name' => 'Machop',
          'pokedexNumber' => '66',
          'description' => 'Very powerful in spite of its small size. Its mastery of many types of martial arts makes it very tough.'
        ),array(
          'name' => 'Machoke',
          'pokedexNumber' => '67',
          'description' => 'The belt around its waist holds back its energy. Without it, this Pokémon would be unstoppable.'
        ),array(
          'name' => 'Machamp',
          'pokedexNumber' => '68',
          'description' => 'One arm alone can move mountains. Using all four arms, this Pokémon fires off awesome punches.'
        ),array(
          'name' => 'Bellsprout',
          'pokedexNumber' => '69',
          'description' => 'Prefers hot and humid places. It ensnares tiny bugs with its vines and devours them.'
        ),array(
          'name' => 'Weepinbell',
          'pokedexNumber' => '70',
          'description' => 'When hungry, it swallows anything that moves. Its hapless prey is dissolved by strong acids.'
        ),array(
          'name' => 'Victreebel',
          'pokedexNumber' => '71',
          'description' => 'Lures prey with the sweet aroma of honey. Swallowed whole, the prey is dissolved in a day, bones and all.'
        ),array(
          'name' => 'Tentacool',
          'pokedexNumber' => '72',
          'description' => 'It can sometimes be found all dry and shriveled up on a beach. Toss it back into the sea to revive it.'
        ),array(
          'name' => 'Tentacruel',
          'pokedexNumber' => '73',
          'description' => 'Its 80 tentacles can stretch and contract freely. They wrap around prey and weaken it with poison.'
        ),array(
          'name' => 'Geodude',
          'pokedexNumber' => '74',
          'description' => 'Commonly found near mountain trails and the like. If you step on one by accident, it gets angry.'
        ),array(
          'name' => 'Graveler',
          'pokedexNumber' => '75',
          'description' => 'Often seen rolling down mountain trails. Obstacles are just things to roll straight over, not avoid.'
        ),array(
          'name' => 'Golem',
          'pokedexNumber' => '76',
          'description' => "Once it sheds its skin, its body turns tender and whitish. Its hide hardens when it's exposed to air."
        ),array(
          'name' => 'Ponyta',
          'pokedexNumber' => '77',
          'description' => 'Capable of jumping incredibly high. Its hooves and sturdy legs absorb the impact of a hard landing.'
        ),array(
          'name' => 'Rapidash',
          'pokedexNumber' => '78',
          'description' => 'Just loves to run. If it sees something faster than itself, it will give chase at top speed.'
        ),array(
          'name' => 'Slowpoke',
          'pokedexNumber' => '79',
          'description' => 'Incredibly slow and sluggish. It is quite content to loll about without worrying about the time.'
        ),array(
          'name' => 'Slowbro',
          'pokedexNumber' => '80',
          'description' => 'Lives lazily by the sea. If the Shellder on its tail comes off, it becomes a Slowpoke again.'
        ),array(
          'name' => 'Magnemite',
          'pokedexNumber' => '81',
          'description' => 'It is hatched with the ability to defy gravity. It floats while emitting powerful electromagnetic waves.'
        ),array(
          'name' => 'Magneton',
          'pokedexNumber' => '82',
          'description' => 'Generates strange radio signals. It raises the temperature by 3.6 degrees Fahrenheit within 3,300 feet.'
        ),array(
          'name' => "Farfetch'd",
          'pokedexNumber' => '83',
          'description' => "They live where reedy plants grow. Farfetch'd are rarely seen, so it's thought their numbers are decreasing."
        ),array(
          'name' => 'Doduo',
          'pokedexNumber' => '84',
          'description' => 'Its short wings make flying difficult. Instead, this Pokémon runs at high speed on developed legs.'
        ),array(
          'name' => 'Dodrio',
          'pokedexNumber' => '85',
          'description' => "One of Doduo's two heads splits to form a unique species. It runs close to 40 mph in prairies."
        ),array(
          'name' => 'Seel',
          'pokedexNumber' => '86',
          'description' => 'Loves freezing-cold conditions. Relishes swimming in a frigid climate of around 14 degrees Fahrenheit.'
        ),array(
          'name' => 'Dewgong',
          'pokedexNumber' => '87',
          'description' => 'Its entire body is a snowy white. Unharmed by even intense cold, it swims powerfully in icy waters.'
        ),array(
          'name' => 'Grimer',
          'pokedexNumber' => '88',
          'description' => "Made of congealed sludge. It smells too putrid to touch. Even weeds won't grow in its path."
        ),array(
          'name' => 'Muk',
          'pokedexNumber' => '89',
          'description' => 'Smells so awful, it can cause fainting. Through degeneration of its nose, it lost its sense of smell.'
        ),array(
          'name' => 'Shellder',
          'pokedexNumber' => '90',
          'description' => 'The shell can withstand any attack. However, when it is open, the tender body is exposed.'
        ),array(
          'name' => 'Cloyster',
          'pokedexNumber' => '91',
          'description' => 'For protection, it uses its harder-than-diamonds shell. It also shoots spikes from the shell.'
        ),array(
          'name' => 'Gastly',
          'pokedexNumber' => '92',
          'description' => 'Said to appear in decrepit, deserted buildings. It has no real shape, as it appears to be made of a gas.'
        ),array(
          'name' => 'Haunter',
          'pokedexNumber' => '93',
          'description' => "By licking, it saps the victim's life. It causes shaking that won't stop until the victim's demise."
        ),array(
          'name' => 'Gengar',
          'pokedexNumber' => '94',
          'description' => 'A Gengar is close by if you feel a sudden chill. It may be trying to lay a curse on you.'
        ),array(
          'name' => 'Onix',
          'pokedexNumber' => '95',
          'description' => 'Burrows at high speed in search of food. The tunnels it leaves are used as homes by Diglett.'
        ),array(
          'name' => 'Drowzee',
          'pokedexNumber' => '96',
          'description' => 'If you sleep by it all the time, it will sometimes show you dreams it had eaten in the past.'
        ),array(
          'name' => 'Hypno',
          'pokedexNumber' => '97',
          'description' => 'Avoid eye contact if you come across one. It will try to put you to sleep by using its pendulum.'
        ),array(
          'name' => 'Krabby',
          'pokedexNumber' => '98',
          'description' => 'Its pincers are superb weapons. They sometimes break off during battle, but they grow back fast.'
        ),array(
          'name' => 'Kingler',
          'pokedexNumber' => '99',
          'description' => 'One claw grew massively and is as hard as steel. It has 10,000-horsepower strength. However, it is too heavy.'
        ),array(
          'name' => 'Voltorb',
          'pokedexNumber' => '100',
          'description' => 'It is said to camouflage itself as a Poké Ball. It will self-destruct with very little stimulus.'
        ),array(
          'name' => 'Electrode',
          'pokedexNumber' => '101',
          'description' => 'Stores electrical energy inside its body. Even the slightest shock could trigger a huge explosion.'
        ),array(
          'name' => 'mew',
          'pokedexNumber' => '151',
          'description' => "When viewed through a microscope, this Pokémon's short, fine, delicate hair can be seen.",
        ),array(
          'name' => 'mewtwo',
          'pokedexNumber' => '150',
          'description' => "Its DNA is almost the same as Mew's. However, its size and disposition are vastly different.",
        ),
array(
      'name' => 'dragonite',
    'pokedexNumber' => '149',
        'description' => 	'It is said that this Pokémon lives somewhere in the sea and that it flies. However, these are only rumors.',
        ),
array(
              'name' => 'dragonair',
            'pokedexNumber' => '148',
                'description' => 'According to a witness, its body was surrounded by a strange aura that gave it a mystical look.',
                ),
array(
                      'name' => 'dratini',
                    'pokedexNumber' => '147',
                        'description' => "Long thought to be a myth, this Pokémon's existence was only recently confirmed by a fisherman who caught one.",
                        ),
array(
          'name' => 'moltres',
          'pokedexNumber' => '146',
        'description' => 'A legendary bird Pokémon. As it flaps its flaming wings, even the night sky will turn red.',
                                                ),
array(
    'name' => 'Zapdos',
    'pokedexNumber' => '145',
    'description' => 'This legendary bird Pokémon is said to appear when the sky turns dark and lightning showers down.',
    ),
array(
    'name' => 'Articuno',
    'pokedexNumber' => '144',
    'description' => 'A legendary bird Pokémon. It freezes water that is contained in winter air and makes it snow.',
    ),
    array(
    'name' => 'Snorlax',
    'pokedexNumber' => '143',
    'description' => 'Will eat anything, even if the food happens to be a little moldy. It never gets an upset stomach.',
  ),
  array(
  'name' => 'Aerodactyl',
  'pokedexNumber' => '142',
  'description' => 'The power of Mega Evolution has completely restored its genes. The rocks on its body are harder than diamond.',
),
array(
'name' => 'Kabutops',
'pokedexNumber' => '141',
'description' => 'A slim and fast swimmer. It sliced its prey with its sharp sickles and drank the body fluids.',
),
array(
'name' => 'Kabuto',
'pokedexNumber' => '140',
'description' => 'A Pokémon that was recovered from a fossil. It used the eyes on its back while hiding on the seafloor',
),
array(
'name' => 'Omastar',
'pokedexNumber' => '139',
'description' => 'Its sharp beak rings its mouth. Its shell was too big for it to move freely, so it became extinct.',
),array(
'name' => 'Omanyte',
'pokedexNumber' => '138',
'description' => 'An ancient Pokémon that was recovered from a fossil. It swam by cleverly twisting its 10 tentacles about.',
),array(
'name' => 'Porygon',
'pokedexNumber' => '137',
'description' => 'The only Pokémon that people anticipate can fly into space. None has managed the feat yet, however.',
),
array(
'name' => 'Flareon',
'pokedexNumber' => '136',
'description' => 'It has a flame chamber inside its body. It inhales, then breathes out fire that is over 3,000 degrees Fahrenheit.',
),
array(
'name' => 'Jolteon',
'pokedexNumber' => '135',
'description' => 'A sensitive Pokémon that easily becomes sad or angry. Every time its mood changes, it charges power.',
),
array(
'name' => 'Vaporeon',
'pokedexNumber' => '134',
'description' => 'Its cell structure is similar to water molecules. It melts into the water and becomes invisible.',
),
array(
'name' => 'Eevee',
'pokedexNumber' => '133',
'description' => "It can evolve into a variety of forms. Eevee's genes are the key to solving the mysteries of Pokémon evolution.",
),
array(
'name' => 'Ditto',
'pokedexNumber' => '132',
'description' => 'When it spots an enemy, its body transfigures into an almost-perfect copy of its opponent.',
),
array(
'name' => 'Lapras',
'pokedexNumber' => '131',
'description' => 'A gentle soul that can understand human speech. It can ferry people across the sea on its back.',
),
array(
'name' => 'Gyarados',
'pokedexNumber' => '130',
'description' => 'Although it obeys its instinctive drive to destroy everything within its reach, it will respond to orders from a Trainer it truly trusts.',
),
array(
'name' => 'Magikarp',
'pokedexNumber' => '129',
'description' => 'Famous for being very unreliable. It can be found swimming in seas, lakes, rivers and shallow puddles.',
),
array(
'name' => 'Tauros',
'pokedexNumber' => '128',
'description' => "A rowdy Pokémon with a lot of stamina. Once running, it won't stop until it hits something.",
),
array(
'name' => 'Pinsir',
'pokedexNumber' => '127',
'description' => 'With its vaunted horns, it can lift an opponent 10 times heavier than itself and fly about with ease.',
),
array(
'name' => 'Magmar',
'pokedexNumber' => '126',
'description' => 'Born in an active volcano. Its body is always cloaked in flames, so it looks like a big ball of fire.',
),
array(
'name' => 'Electabuzz',
'pokedexNumber' => '125',
'description' => 'If a major power outage occurs, it is certain that this Pokémon has eaten electricity at a power plant.',
),
array(
'name' => 'Jynx',
'pokedexNumber' => '124',
'description' => 'Appears to move to a rhythm of its own, as if it were dancing. It wiggles its hips as it walks.',
),
array(
'name' => 'Scyther',
'pokedexNumber' => '123',
'description' => 'Leaps out of tall grass and slices prey with its scythes. The movement looks like that of a ninja.',
),
array(
'name' => 'Mr.Mime',
'pokedexNumber' => '122',
'description' => "Always practices its pantomime act. It makes enemies believe something exists that really doesn't.",),
array(
'name' => 'Starmie',
'pokedexNumber' => '121',
'description' => 'The center section is named the core. People think it is communicating when it glows in seven colors.',
),
array(
'name' => 'Staryu',
'pokedexNumber' => '120',
'description' => 'As long as the center section is unharmed, this Pokémon can grow back fully even if it is chopped to bits.',
),
array(
'name' => 'Seaking',
'pokedexNumber' => '119',
'description' => "It is the male's job to make a nest by carving out boulders in a stream using the horn on its head.",
),
array(
'name' => 'Goldeen',
'pokedexNumber' => '118',
'description' => 'When it is time for them to lay eggs, they can be seen swimming up rivers and falls in large groups.',
),
array(
'name' => 'Seadra',
'pokedexNumber' => '117',
'description' => 'Touching the back fin causes numbness. It hooks its tail to coral to stay in place while sleeping.',
),
array(
'name' => 'Horsea',
'pokedexNumber' => '116',
'description' => 'If it senses any danger, it will vigorously spray water or a special type of ink from its mouth.',
),
array(
'name' => 'Kangaskhan',
'pokedexNumber' => '115',
'description' => 'Its child has grown rapidly, thanks to the energy of Mega Evolution. Mother and child show their harmonious teamwork in battle.',
),
array(
'name' => 'Tangela',
'pokedexNumber' => '114',
'description' => 'Its identity is obscured by masses of thick blue vines. The vines are said to never stop growing.',
),
array(
'name' => 'Chansey',
'pokedexNumber' => '113',
'description' => 'A gentle and kindhearted Pokémon that shares its nutritious eggs if it sees an injured Pokémon.',
),
array(
'name' => 'Rhydon',
'pokedexNumber' => '112',
'description' => 'Its brain developed when it began walking on its hind legs. Its armor-like hide even repels molten lava.',
),
array(
'name' => 'Rhyhorn',
'pokedexNumber' => '111',
'description' => "A Pokémon with a one-track mind. Once it charges, it won't stop running until it falls asleep.",
),
array(
'name' => 'Weezing',
'pokedexNumber' => '110',
'description' => 'This Pokémon lives and grows by absorbing poison gas, dust, and germs that exist inside garbage.',
),
array(
'name' => 'Koffing',
'pokedexNumber' => '109',
'description' => 'In hot places, its internal gases could expand and explode without any warning. Be very careful!',
),
array(
'name' => 'Lickitung',
'pokedexNumber' => '108',
'description' => 'Its tongue spans almost seven feet and moves more freely than its forelegs. Its licks can cause paralysis.',
),
array(
'name' => 'Hitmonchan',
'pokedexNumber' => '107',
'description' => 'Punches in corkscrew fashion. It can punch its way through a concrete wall like a drill.',
),
array(
'name' => 'Hitmonlee',
'pokedexNumber' => '106',
'description' => 	'When kicking, the sole of its foot turns as hard as a diamond on impact and destroys its enemy.',
),
array(
'name' => 'Marowak',
'pokedexNumber' => '105',
'description' => 'It has transformed the spirit of its dear departed mother into flames, and tonight it will once again dance in mourning of others of its kind.',
),
array(
'name' => 'Cubone',
'pokedexNumber' => '104',
'description' => 'Wears the skull of its deceased mother. Its cries echo inside the skull and come out as a sad melody.',
),
array(
'name' => 'Exeggutor',
'pokedexNumber' => '103',
'description' => 'The strong sunlight of the Alola region has awakened the power hidden within Exeggcute. This is the result.',
),array(
'name' => 'Exeggcute',
'pokedexNumber' => '102',
'description' => 'The heads attract each other and spin around. There must be six heads for it to maintain balance.',
)
      ]);
    }

    public function createLocation(){
      $factory = (new Factory)->withServiceAccount(__DIR__.'/laravelfirebasepokemon-firebase-adminsdk-3zf5c-dcd1ced1cf.json');

      $database = $factory->createDatabase();

      $newPost = $database
      ->getReference('Location')
      ->push([
        array(
          'pokedex_id' => '1',
          'generation' => '7',
          'location' => 'LGP\LGE: Viridian Forest Cerulean City(only one)'
        ), array(
          'pokedex_id' => '2',
          'generation' => '7',
          'location' => 'LGP\LGE: Evolve Bulbasaur'
        ), array(
          'pokedex_id' => '3',
          'generation' => '7',
          'location' => 'LGP\LGE: Evolve Ivysaur'
        ),array(
          'pokedex_id' => '4',
          'generation' => '7',
          'location' => 'LGP\LGE: Route 3, Route 4, Rock Tunnel, Route 24 (only one)'
        ),array(
          'pokedex_id' => '5',
          'generation' => '7',
          'location' => 'LGP\LGE: Evolve Charmander'
        ),array(
          'pokedex_id' => '6',
          'generation' => '7',
          'location' => 'LGP\LGE: Routes 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 21, 22, 23, 24, and 25'
        ),array(
          'pokedex_id' => '7',
          'generation' => '7',
          'location' => 'LGP\LGE: Route 24, Route 25, Seafoam Islands, Vermilion City (only one)'
        ),array(
          'pokedex_id' => '8',
          'generation' => '7',
          'location' => 'LGP\LGE: Evolve Squirtle'
        ),array(
          'pokedex_id' => '9',
          'generation' => '7',
          'location' => 'LGP\LGE: Evolve Wartortle'
        ),array(
          'pokedex_id' => '10',
          'generation' => '7',
          'location' => 'LGP\LGE: Route 2, Viridian Forest'
        ),array(
          'pokedex_id' => '11',
          'generation' => '7',
          'location' => 'LGP\LGE: Viridian Forest'
        ),array(
          'pokedex_id' => '12',
          'generation' => '7',
          'location' => 'LGP\LGE: Evolve Metapod'
        ),array(
          'pokedex_id' => '13',
          'generation' => '7',
          'location' => 'LGP\LGE: Route 2, Viridian Forest'
        ),array(
          'pokedex_id' => '14',
          'generation' => '7',
          'location' => 'LGP\LGE: Viridian Forest'
        ),array(
          'pokedex_id' => '15',
          'generation' => '7',
          'location' => 'LGE: Viridian Forest'
        ),array(
          'pokedex_id' => '16',
          'generation' => '7',
          'location' => 'LGP\LGE: Routes 1, 2, 5, 6, 7, 8, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 24, and 25, Viridian Forest'
        ),array(
          'pokedex_id' => '17',
          'generation' => '7',
          'location' => 'LGP\LGE: Routes 1, 2, 5, 6, 7, 8, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 24, and 25'
        ),array(
          'pokedex_id' => '18',
          'generation' => '7',
          'location' => 'LGP\LGE: Routes 1, 2, 5, 6, 7, 8, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 24, and 25'
        ),array(
          'pokedex_id' => '19',
          'generation' => '7',
          'location' => 'LGP\LGE: Routes 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 16, 17, 18, 21, and 22, Pokemon Mansion'
        ),array(
          'pokedex_id' => '20',
          'generation' => '7',
          'location' => 'LGP\LGE: Routes 7, 8, 9, 10, 11, 16, 17, 18 and 21, Pokémon Mansion'
        ),array(
          'pokedex_id' => '21',
          'generation' => '7',
          'location' => 'LGP\LGE: Routes 3, 4, 9, 10, 22, and 23'
        ),array(
          'pokedex_id' => '22',
          'generation' => '7',
          'location' => 'LGP\LGE: Routes 3, 4, 9, 10, 22, and 23'
        ),array(
          'pokedex_id' => '23',
          'generation' => '7',
          'location' => 'LGE: Routes 3 and 4'
        ),array(
          'pokedex_id' => '24',
          'generation' => '7',
          'location' => 'LGE: Evolve Ekans'
        ),array(
          'pokedex_id' => '25',
          'generation' => '7',
          'location' => 'LGP\LGE: Viridian Forest (Pikachu),
          Starter Pokémon from Professor Oak in Pallet Town '
        ),array(
          'pokedex_id' => '26',
          'generation' => '7',
          'location' => 'LGP\LGE: Evolve Pikachu '
        ),array(
          'pokedex_id' => '27',
          'generation' => '7',
          'location' => 'LGP: Routes 3 and 4'
        ),array(
          'pokedex_id' => '28',
          'generation' => '7',
          'location' => 'LGP: Evolve Sandshrew'
        ),array(
          'pokedex_id' => '29',
          'generation' => '7',
          'location' => 'LGP\LGE: Routes 9, 10, 22 and 23'
        ),array(
          'pokedex_id' => '30',
          'generation' => '7',
          'location' => 'LGP\LGE: Routes 9, 10, and 23'
        ),array(
          'pokedex_id' => '31',
          'generation' => '7',
          'location' => 'LGP\LGE: Route 23'
        ),array(
          'pokedex_id' => '32',
          'generation' => '7',
          'location' => 'LGP\LGE: Routes 9, 10, 22 and 23'
        ),array(
          'pokedex_id' => '33',
          'generation' => '7',
          'location' => 'LGP\LGE: Routes 9, 10 and 23'
        ),array(
          'pokedex_id' => '34',
          'generation' => '7',
          'location' => 'LGP\LGE: Route 23'
        ),array(
          'pokedex_id' => '35',
          'generation' => '7',
          'location' => 'LGP\LGE: Mt. Moon'
        ),array(
          'pokedex_id' => '36',
          'generation' => '7',
          'location' => 'LGP\LGE: Mt. Moon'
        ),array(
          'pokedex_id' => '37',
          'generation' => '7',
          'location' => 'LGP\LGE: Routes 5, 6, 7 and 8'
        ),array(
          'pokedex_id' => '38',
          'generation' => '7',
          'location' => 'LGP\LGE: Routes 7 and 8'
        ),array(
          'pokedex_id' => '39',
          'generation' => '7',
          'location' => 'LGP\LGE: Routes 5, 6, 7 and 8'
        ),array(
          'pokedex_id' => '40',
          'generation' => '7',
          'location' => 'LGP\LGE: Evolve Jigglypuff'
        ),array(
          'pokedex_id' => '41',
          'generation' => '7',
          'location' => "LGP\LGE: Mt. Moon, Diglett's Cave, Rock Tunnel, Pokémon Tower, Seafoam Islands, Victory Road, Cerulean Cave"
        ),array(
          'pokedex_id' => '42',
          'generation' => '7',
          'location' => "LGP\LGE: Rock Tunnel, Pokémon Tower, Seafoam Islands, Victory Road, Cerulean Cave"
        ),array(
          'pokedex_id' => '43',
          'generation' => '7',
          'location' => 'LGP: Routes 1, 2, 12, 13, 14, 15, 21, 24, and 25, Viridian Forest'
        ),array(
          'pokedex_id' => '44',
          'generation' => '7',
          'location' => 'LGP: Routes 12, 13, 14, 15 and 21'
        ),array(
          'pokedex_id' => '45',
          'generation' => '7',
          'location' => 'LGP: Route 21'
        ),array(
          'pokedex_id' => '46',
          'generation' => '7',
          'location' => 'LGP\LGE: Mt. Moon'
        ),array(
          'pokedex_id' => '47',
          'generation' => '7',
          'location' => 'LGP\LGE: Evolve Paras'
        ),array(
          'pokedex_id' => '48',
          'generation' => '7',
          'location' => 'LGP\LGE: Routes 14, 15, 24 and 25'
        ),array(
          'pokedex_id' => '49',
          'generation' => '7',
          'location' => 'LGP\LGE: Routes 14 and 15'
        ),array(
          'pokedex_id' => '151',
          'generation' => '7',
          'location' => 'LGP\LGE: Transfer from Poké Ball Plus'
        ),
array(
                  'pokedex_id' => '150',
                  'generation' => '7',
                  'location' => 'LGP\LGE: Cerulean Cave (only one)'
                ),
  array(
              'pokedex_id' => '149',
                    'generation' => '7',
      'location' => 'LGP\LGE: Routes 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 21, 22, 23, 24, and 25'
  ),
  array(
              'pokedex_id' => '148',
                    'generation' => '7',
      'location' => 'LGP\LGE: Route 10 (Sea Skim)'
  ),
  array(
              'pokedex_id' => '147',
                    'generation' => '7',
      'location' => 'LGP\LGE: Route 10 (Sea Skim)'
  ),
  array(
              'pokedex_id' => '146',
                    'generation' => '7',
      'location' => 'LGP\LGE: Victory Road (Only one)
Routes 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 21, 22, 23, 24, and 25'
  ),
  array(
              'pokedex_id' => '145',
                    'generation' => '7',
      'location' => 'LGP\LGE: Power Plant (Only one)
Routes 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 21, 22, 23, 24, and 25'
  ),
  array(
              'pokedex_id' => '144',
                    'generation' => '7',
      'location' => 'LGP\LGE: Seafoam Islands (Only one)
Routes 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 21, 22, 23, 24, and 25'
  ),
  array(
              'pokedex_id' => '143',
                    'generation' => '7',
      'location' => 'LGP\LGE: Routes 12 and 16 (only two)
Cerulean Cave'
  ),
  array(
              'pokedex_id' => '142',
                    'generation' => '7',
      'location' => 'LGP\LGE: Revive from Old Amber at the Cinnabar Lab on Cinnabar Island'
  ),
  array(
              'pokedex_id' => '141',
                    'generation' => '7',
      'location' => 'LGP\LGE: Evolve Kabuto'
  ),
  array(
              'pokedex_id' => '140',
                    'generation' => '7',
      'location' => 'LGP\LGE: Revive from Old Amber at the Cinnabar Lab on Cinnabar Island'
  ),
  array(
              'pokedex_id' => '139',
                    'generation' => '7',
      'location' => 'LGP\LGE: Evolve Omanyte'
  ),
  array(
              'pokedex_id' => '138',
                    'generation' => '7',
      'location' => 'LGP\LGE: Revive from Old Amber at the Cinnabar Lab on Cinnabar Island'
  ),
  array(
              'pokedex_id' => '137',
                    'generation' => '7',
      'location' => 'LGP\LGE: Route 7
Received from a person in Saffron City'
  ),
  array(
              'pokedex_id' => '136',
                    'generation' => '7',
      'location' => 'LGP\LGE:evolve Eevee'
  ),
  array(
              'pokedex_id' => '135',
                    'generation' => '7',
      'location' => 'LGP\LGE: evolve Eevee'
  ),
  array(
              'pokedex_id' => '134',
                    'generation' => '7',
      'location' => 'LGP\LGE: evolve Eevee'
  ),
  array(
              'pokedex_id' => '133',
                    'generation' => '7',
      'location' => 'LGP\LGE: Route 17 (Eevee)
Starter Pokémon from Professor Oak in Pallet Town (Partner Eevee)'
  ),
  array(
              'pokedex_id' => '132',
                    'generation' => '7',
      'location' => 'LGP\LGE: Pokémon Mansion and Cerulean Cave)'
  ),
  array(
              'pokedex_id' => '131',
                    'generation' => '7',
      'location' => 'LGP\LGE: Routes 19 and 20
Received from a person in Silph Co. (Saffron City)'
  ),
  array(
              'pokedex_id' => '130',
                    'generation' => '7',
      'location' => 'LGP\LGE: Route 20 (Sea Skim)'
  ),
  array(
              'pokedex_id' => '129',
                    'generation' => '7',
      'location' => 'LGP\LGE: Routes 4, 6, 10, 11, 12, 13, 18, 19, 20, 21, 22, 23, 24, and 25, Seafoam Islands, Cerulean Cave (Sea Skim)
Buy from Magikarp salesman for 500 on Route 4'
  ),
  array(
              'pokedex_id' => '128',
                    'generation' => '7',
      'location' => 'LGP\LGE:	Routes 14 and 15'
  ),
  array(
              'pokedex_id' => '127',
                    'generation' => '7',
      'location' => 'LGP\LGE:	Routes 14 and 15'
  ),
  array(
              'pokedex_id' => '126',
                    'generation' => '7',
      'location' => 'LGP\LGE:	Pokémon Mansion'
  ),
  array(
              'pokedex_id' => '125',
                    'generation' => '7',
      'location' => 'LGP\LGE:	Power Plant'
  ),
  array(
              'pokedex_id' => '124',
                    'generation' => '7',
      'location' => 'LGP\LGE: Seafoam Islands'
  ),
  array(
              'pokedex_id' => '123',
                    'generation' => '7',
      'location' => 'LGP\LGE: Routes 14 and 15'
  ),
  array(
              'pokedex_id' => '122',
                    'generation' => '7',
      'location' => 'LGP\LGE: Route 11'
  ),
  array(
              'pokedex_id' => '121',
                    'generation' => '7',
      'location' => 'LGP\LGE: Routes 18, 19, and 21 (Sea Skim)'
  ),
  array(
              'pokedex_id' => '120',
                    'generation' => '7',
      'location' => 'LGP\LGE: Routes 18, 19, and 21 (Sea Skim)'
  ),
  array(
              'pokedex_id' => '119',
                    'generation' => '7',
      'location' => 'LGP\LGE: Route 6 (Sea Skim)'
  ),
  array(
              'pokedex_id' => '118',
                    'generation' => '7',
      'location' => 'LGP\LGE: Route 6 (Sea Skim)'
  ),
  array(
              'pokedex_id' => '117',
                    'generation' => '7',
      'location' => 'LGP\LGE: Routes 11, 12, and 13 (Sea Skim)'
  ),
  array(
              'pokedex_id' => '116',
                    'generation' => '7',
      'location' => 'LGP\LGE: Routes 11, 12, and 13 (Sea Skim)'
  ),
  array(
              'pokedex_id' => '115',
                    'generation' => '7',
      'location' => 'LGP\LGE: Rock Tunnel'
  ),
  array(
              'pokedex_id' => '114',
                    'generation' => '7',
      'location' => 'LGP\LGE: Route 21'
  ),
  array(
              'pokedex_id' => '113',
                    'generation' => '7',
      'location' => "LGP\LGE: Routes 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 21 and 23, Diglett's Cave, Mt. Moon, Pokémon Tower, Power Plant, Pokémon Mansion, Cerulean Cave"
  ),
  array(
              'pokedex_id' => '112',
                    'generation' => '7',
      'location' => 'LGP\LGE: Victory Road, Cerulean Cave'
  ),
  array(
              'pokedex_id' => '111',
                    'generation' => '7',
      'location' => 'LGP\LGE: Rock Tunnel, Victory Road, Cerulean Cave'
  ),
  array(
              'pokedex_id' => '110',
                    'generation' => '7',
      'location' => 'LGP\LGE: Power Plant, Pokémon Mansion'
  ),
  array(
              'pokedex_id' => '109',
                    'generation' => '7',
      'location' => 'LGP\LGE: Power Plant, Pokémon Mansion'
  ),

  array(
              'pokedex_id' => '108',
                    'generation' => '7',
      'location' => 'LGP\LGE: Cerulean Cave'
  ),
  array(
              'pokedex_id' => '107',
                    'generation' => '7',
      'location' => 'LGP\LGE: Received from the master of the Fighting Dojo in Saffron City (choice between it and Hitmonlee) Victory Road'
  ),
  array(
              'pokedex_id' => '106',
                    'generation' => '7',
      'location' => 'LGP\LGE: Received from the master of the Fighting Dojo in Saffron City (choice between it and Hitmonlee) Victory Road'
  ),
  array(
              'pokedex_id' => '105',
                    'generation' => '7',
      'location' => 'LGP\LGE:Evolve Cubone Kantonian Form Trade Marowak in Fuchsia City Alolan Form'
),
array(
            'pokedex_id' => '104',
            'generation' => '7',
        'location' => 'LGP\LGE:Rock Tunnel, Pokémon Tower'
),
array(
            'pokedex_id' => '103',
            'generation' => '7',
        'location' => 'LGP\LGE:Route 23 Kantonian Form Trade Exeggutor at the Pokémon League Alolan Form'
),
array(
            'pokedex_id' => '102',
            'generation' => '7',
        'location' => 'LGP\LGE:Route 23'
),
array(
            'pokedex_id' => '101',
            'generation' => '7',
        'location' => 'LGP\LGE:Power Plant'
),
array(
            'pokedex_id' => '100',
            'generation' => '7',
        'location' => 'LGP\LGE:Power Plant'
),
array(
            'pokedex_id' => '99',
            'generation' => '7',
        'location' => 'LGP\LGE:Routes 12 and 13'
),
array(
            'pokedex_id' => '98',
            'generation' => '7',
        'location' => 'LGP\LGE:Routes 10, 12 and 13'
),
array(
            'pokedex_id' => '97',
            'generation' => '7',
        'location' => 'LGP\LGE: Evolve Drowzee'
),
array(
            'pokedex_id' => '96',
            'generation' => '7',
        'location' => 'LGP\LGE: Route 11'
),
array(
            'pokedex_id' => '95',
            'generation' => '7',
        'location' => 'LGP\LGE: Mt. Moon, Rock Tunnel, Victory Road'
),
array(
            'pokedex_id' => '94',
            'generation' => '7',
        'location' => 'LGP\LGE: Evolve Haunter'
),
array(
            'pokedex_id' => '93',
            'generation' => '7',
        'location' => 'LGP\LGE: Pokémon Tower'
),
array(
            'pokedex_id' => '92',
            'generation' => '7',
        'location' => 'LGP\LGE: Pokémon Tower'
),
array(
            'pokedex_id' => '91',
            'generation' => '7',
        'location' => 'LGP\LGE: Seafoam Islands (Sea Skim)'
),
array(
            'pokedex_id' => '90',
            'generation' => '7',
        'location' => 'LGP\LGE: Seafoam Islands (Sea Skim)'
),
array(
            'pokedex_id' => '89',
            'generation' => '7',
        'location' => 'LGP\LGE: Pokémon Mansion, Power Plant Kanto Form Evolve Grimer Alolan Form'
),
array(
            'pokedex_id' => '88',
            'generation' => '7',
        'location' => 'LGP\LGE: Pokémon Mansion, Power Plant Kanto Form Trade Grimer in Cinnabar Island Alolan Form'
),
array(
            'pokedex_id' => '87',
            'generation' => '7',
        'location' => 'LGP\LGE: Seafoam Islands'
),
array(
            'pokedex_id' => '86',
            'generation' => '7',
        'location' => 'LGP\LGE: Seafoam Islands'
),
array(
            'pokedex_id' => '85',
            'generation' => '7',
        'location' => 'LGP\LGE: Routes 16, 17 and 18'
),
array(
            'pokedex_id' => '84',
            'generation' => '7',
        'location' => 'LGP\LGE: Routes 16, 17 and 18'
),
array(
            'pokedex_id' => '83',
            'generation' => '7',
        'location' => 'LGP\LGE: Routes 12 and 13'
),
array(
            'pokedex_id' => '82',
            'generation' => '7',
        'location' => 'LGP\LGE: Power Plant'
),
array(
            'pokedex_id' => '81',
            'generation' => '7',
        'location' => 'LGP\LGE: Power Plant'
),
array(
            'pokedex_id' => '80',
            'generation' => '7',
        'location' => 'LGP\LGE: Seafoam Islands'
),
array(
            'pokedex_id' => '79',
            'generation' => '7',
        'location' => 'LGP\LGE: Seafoam Islands'
),
array(
            'pokedex_id' => '78',
            'generation' => '7',
        'location' => 'LGP\LGE: Route 17'
),
array(
            'pokedex_id' => '77',
            'generation' => '7',
        'location' => 'LGP\LGE: Route 17'
),
array(
            'pokedex_id' => '76',
            'generation' => '7',
        'location' => 'LGP\LGE: Evolve Graveler'
),
array(
            'pokedex_id' => '75',
            'generation' => '7',
        'location' => 'LGP\LGE: Rock Tunnel, Victory Road, Cerulean Cave Kanto Form Evolve Geodude Alolan Form'
),
array(
            'pokedex_id' => '74',
            'generation' => '7',
        'location' => 'LGP\LGE: Mt. Moon, Rock Tunnel, Victory Road, Cerulean Cave Kanto Form Trade Geodude in Vermilion City Alolan Form'
),
array(
            'pokedex_id' => '73',
            'generation' => '7',
        'location' => 'LGP\LGE: Routes 4, 10, 12, 13, 18, 19, 20, 21 and 24, Seafoam Islands (Sea Skim)'
),
array(
            'pokedex_id' => '72',
            'generation' => '7',
        'location' => 'LGP\LGE: Routes 4, 10, 12, 13, 18, 19, 20, 21 and 24, Seafoam Islands (Sea Skim)'
),
array(
            'pokedex_id' => '71',
            'generation' => '7',
        'location' => 'LGP\LGE: Route 21'
),
array(
            'pokedex_id' => '70',
            'generation' => '7',
        'location' => 'LGP\LGE: Routes 12, 13, 14, 15 and 21'
),
array(
            'pokedex_id' => '69',
            'generation' => '7',
        'location' => 'LGP\LGE: Routes 1, 2, 12, 13, 14, 15, 21, 24 and 25, Viridian Forest'
),
array(
            'pokedex_id' => '68',
            'generation' => '7',
        'location' => 'LGP\LGE: Evolve Machoke'
),
array(
            'pokedex_id' => '67',
            'generation' => '7',
        'location' => 'LGP\LGE: Victory Road'
),
array(
            'pokedex_id' => '66',
            'generation' => '7',
        'location' => 'LGP\LGE: Rock Tunnel, Victory Road'
),
array(
            'pokedex_id' => '65',
            'generation' => '7',
        'location' => 'LGP\LGE: Evolve Kadabra'
),
array(
            'pokedex_id' => '64',
            'generation' => '7',
        'location' => 'LGP\LGE: Routes 7 and 8'
),
array(
            'pokedex_id' => '63',
            'generation' => '7',
        'location' => 'LGP\LGE: Routes 5, 6, 7 and 8'
),
array(
            'pokedex_id' => '62',
            'generation' => '7',
        'location' => 'LGP\LGE: Cerulean Cave (Sea Skim)'
),
array(
            'pokedex_id' => '61',
            'generation' => '7',
        'location' => 'LGP\LGE: Routes 22, 23 and 25, Cerulean Cave (Sea Skim)'
),
array(
            'pokedex_id' => '60',
            'generation' => '7',
        'location' => 'LGP\LGE: Routes 22, 23 and 25, Cerulean Cave (Sea Skim)'
),
array(
            'pokedex_id' => '59',
            'generation' => '7',
        'location' => 'LGP\LGE: Routes 7 and 8, Vermilion City (only one)'
),
array(
            'pokedex_id' => '58',
            'generation' => '7',
        'location' => 'LGP\LGE: Routes 5, 6, 7 and 8'
),
array(
            'pokedex_id' => '57',
            'generation' => '7',
        'location' => 'LGP\LGE: Evolve Mankey'
),
array(
            'pokedex_id' => '56',
            'generation' => '7',
        'location' => 'LGP\LGE: Routes 3 and 4'
),
array(
            'pokedex_id' => '55',
            'generation' => '7',
        'location' => 'LGP\LGE: Cerulean Cave'
),
array(
            'pokedex_id' => '54',
            'generation' => '7',
        'location' => 'LGP\LGE: Routes 4, 6, 17, 24, 25 and Cerulean Cave'
),
array(
            'pokedex_id' => '53',
            'generation' => '7',
        'location' => 'LGP\LGE: Evolve Meowth Kanto Form Evolve Meowth Alolan Form'
),
array(
            'pokedex_id' => '52',
            'generation' => '7',
        'location' => 'LGP\LGE: Routes 24 and 25 Kantonian Form Trade Meowth on Cinnabar Island Alolan Form'
),
array(
            'pokedex_id' => '51',
            'generation' => '7',
        'location' => "LGP\LGE: Diglett's Cave Kanto Form Evolve Diglett Alolan Form"
),
array(
            'pokedex_id' => '50',
            'generation' => '7',
        'location' => "LGP\LGE: Diglett's Cave Kanto Form Trade Diglett in Lavender Town Alolan Form"
),

      ]);
       return response()->json($newPost->getvalue());
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
