<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pokedex;

class PokedexController extends Controller
{
    public function all(){
      $pokedex = Pokedex::get();
      return response()->json($pokedex);
    }
}
