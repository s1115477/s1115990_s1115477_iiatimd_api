<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Location;

class locationController extends Controller
{
    public function all(){
      $locations = Location::get();
      return response()->json($locations);
    }
}
